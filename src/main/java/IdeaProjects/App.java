package IdeaProjects;

import java.util.Arrays;

public class App
{
    public static void main( String[] args )
    {
        App.sort(new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1});
    }

    public static void sort(int[] array) {
        if (array == null || array.length == 0 || array.length > 10) {
            throw new IllegalArgumentException();
        }
        Arrays.sort(array);
    }
}