package IdeaProjects;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses(
        {SortingAppTestNullCase.class,
                SortingAppTestArraySizeCase.class,
                SortingAppTestUsualCase.class}
)
public class AppTest{}