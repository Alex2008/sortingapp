package IdeaProjects;

import org.junit.Test;

public class SortingAppTestArraySizeCase {

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyCase() {
        App.sort(new int[]{});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOverloadCase() {
        App.sort(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
    }
}
