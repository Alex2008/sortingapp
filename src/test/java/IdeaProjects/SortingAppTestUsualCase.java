package IdeaProjects;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppTestUsualCase {

    private final int[] array;
    private final int[] expected;

    public SortingAppTestUsualCase(int[] array, int[] expected) {
        this.array = array;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> resources() {
        return Arrays.asList(new Object[][] {
                {new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {new int[]{2, 3, 7, 1, 4, 8}, new int[]{1, 2, 3, 4, 7, 8}},
                {new int[]{9, 5, 2, 6, 4}, new int[]{2, 4, 5, 6, 9}}
        });
    }

    @Test
    public void testUsualCase() {
        App.sort(array);
        assertArrayEquals(array, expected);
    }
}
