package IdeaProjects;

import org.junit.Test;

public class SortingAppTestNullCase {

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase() {
        App.sort(null);
    }
}
